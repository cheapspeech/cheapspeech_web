import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/api/providers/remark_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'create_remark_model.dart';

class CreateRemarkController with ChangeNotifier {
  final int imageTooLargeThreshold = 1500000;
  final int videoTooLargeThreshold = 75000000;
  final List<String> useableImageExtensions = [
    "jpg",
    "jpeg",
    "png",
    "ico",
    "gif"
  ];
  final List<String> useableVideoExtensions = [
    "mp4",
  ];

  CreateRemarkModel model = new CreateRemarkModel();
  TextEditingController titleController = new TextEditingController();
  TextEditingController bodyController = new TextEditingController();
  TextEditingController linkUrlController = new TextEditingController();
  TextEditingController linkTextController = new TextEditingController();
  TextEditingController addTagController = new TextEditingController();

  void setRemarkType(RemarkTypes type) {
    this.model.type = type;
    notifyListeners();
  }

  Future<String?> addImageToModel(XFile? image, int index) async {
    if (image == null) {
      return "File could not be loaded.";
    }

    var bytes = await image.readAsBytes();
    var splitName = image.name.split(".");

    if (splitName.length < 2) {
      return "File extension could not be parsed.";
    } else if (this.imageTooLargeThreshold < bytes.length) {
      return "File is too large to upload, limit is 1.5 Mb.";
    }

    var fileExtension = splitName[1];
    fileExtension = fileExtension.toLowerCase();

    if (!this.useableImageExtensions.contains(fileExtension)) {
      return "File with extension ." + fileExtension + " can not be uploaded.";
    }

    this.model.images[index] = new UploadFile(
      bytes: bytes,
      name: "uploadimage." + fileExtension,
    );

    notifyListeners();
  }

  void removeImageFromModel(int index) {
    this.model.images[index] = null;
    notifyListeners();
  }

  Future<String?> addVideoToModel(XFile? video) async {
    if (video == null) {
      return "Video could not be loaded.";
    }

    var bytes = await video.readAsBytes();
    var splitName = video.name.split(".");

    if (splitName.length < 2) {
      return "Video extension could not be parsed.";
    } else if (this.videoTooLargeThreshold < bytes.length) {
      return "Video is too large to upload, limit is 75 Mb.";
    }

    var fileExtension = splitName[1];
    fileExtension = fileExtension.toLowerCase();
    if (!this.useableVideoExtensions.contains(fileExtension)) {
      return "File with extension ." + fileExtension + " can not be uploaded.";
    }

    this.model.video = new UploadFile(
      bytes: bytes,
      name: "uploadvideo." + fileExtension,
    );

    notifyListeners();
  }

  Future<bool> submitRemark() async {
    bool recordCreationSuccess = await this.createRemarkRecord();

    if (!recordCreationSuccess) {
      return false;
    }

    if (this.model.type != RemarkTypes.text) {
      bool fileUploadSuccess = await this.uploadFiles();
      return fileUploadSuccess;
    } else {
      return true;
    }
  }

  Future<bool> createRemarkRecord() async {
    var title = this.titleController.text;
    var body = this.bodyController.text;
    var model = new RemarkModel();
    model.title = title;
    model.text = body;
    model.type = this.model.type;
    model.tags = this.model.tags;
    model.parentId = this.model.parentId;

    var provider = new RemarkProvider();
    var response = await provider.createRemarkRecord(model);
    this.model.remarkId = response.remarkId;
    return response.remarkId != null;
  }

  Future<bool> uploadFiles() async {
    if (this.model.type == RemarkTypes.images) {
      var failedUploads = [];
      var provider = new RemarkProvider();
      this.model.images.forEach(
        (element) async {
          if (element == null) {
            return;
          }
          var success =
              await provider.uploadFile(element, this.model.remarkId!);
          if (!success) {
            failedUploads.add(element.name);
          }
        },
      );
      return failedUploads.length == 0;
    } else if (this.model.type == RemarkTypes.videos) {
      var provider = new RemarkProvider();
      var success = await provider.uploadFile(
        this.model.video,
        this.model.remarkId!,
      );
      return success;
    } else {
      return false;
    }
  }

  void addLink() {
    var linkText = this.linkTextController.text;
    var linkUrl = this.linkUrlController.text;
    var fullLink = ' [$linkText]($linkUrl) ';
    this.bodyController.text += fullLink;
    this.model.showLinkCreator = false;
    notifyListeners();
  }

  void toggleLinkCreator({bool? show}) {
    if (show == null) {
      this.model.showLinkCreator = !this.model.showLinkCreator;
    } else {
      this.model.showLinkCreator = show;
    }

    notifyListeners();
  }

  void addTag() {
    var tags = this.addTagController.text;
    var splitTags = tags.split(" ");
    splitTags.forEach((element) {
      var tag = element.replaceAll("#", "").toLowerCase();
      if (!this.model.tags.contains(tag) && tag.isNotEmpty) {
        this.model.tags.add(tag);
      }
    });

    notifyListeners();
  }

  void removeTag(String tag) {
    this.model.tags.removeWhere((element) => element == tag);
    notifyListeners();
  }
}

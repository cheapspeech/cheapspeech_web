import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/pages/create_remark/create_remark_controller.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:cheapspeech_web/widgets/tag_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class CreateRemark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var parentId = ModalRoute.of(context)!.settings.arguments;
    if (!(parentId is String?)) {
      print(
          "CreateRemark widget must recieve route arguments of type String? !! Popping route.");
      Navigator.of(context).maybePop();
      return Container();
    }

    return ChangeNotifierProvider(
      child: SharedScaffoldFactory.get(
        context: context,
        showBackArrow: true,
        body: Consumer<CreateRemarkController>(
          builder: (context, controller, child) {
            return this.getBuildStage(context, controller);
          },
        ),
      ),
      create: (context) {
        var controller = CreateRemarkController();
        controller.model.parentId = parentId;
        return controller;
      },
    );
  }

  Widget getBuildStage(
      BuildContext context, CreateRemarkController controller) {
    if (controller.model.type == RemarkTypes.unselected) {
      return this.selectRemarkTypeStage(controller, context);
    } else {
      return this.createPostLocally(context, controller);
    }
  }

  Widget selectRemarkTypeStage(
    CreateRemarkController controller,
    BuildContext context,
  ) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
              child: SelectableText(
                "Which one, which one...",
                style: TextStyle(
                  fontSize: 35,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            this.makeTypeSelectionButton(
              tooltip: "Text Remark",
              icon: Icons.text_snippet,
              onPressed: () {
                controller.setRemarkType(RemarkTypes.text);
              },
            ),
            this.makeTypeSelectionButton(
              tooltip: "Image Remark",
              icon: Icons.image,
              onPressed: () {
                controller.setRemarkType(RemarkTypes.images);
              },
            ),
            this.makeTypeSelectionButton(
              tooltip: "Video Remark",
              icon: Icons.video_camera_back,
              onPressed: () {
                controller.setRemarkType(RemarkTypes.videos);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget makeTypeSelectionButton(
      {required String tooltip,
      required IconData icon,
      required Function onPressed}) {
    return SizedBox(
      width: 250,
      child: Tooltip(
        message: tooltip,
        child: Card(
          child: InkWell(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 30),
                  child: Icon(
                    icon,
                    size: 50,
                  ),
                ),
              ],
            ),
            onTap: () => onPressed(),
          ),
        ),
      ),
    );
  }

  Widget createPostLocally(
      BuildContext context, CreateRemarkController controller) {
    var widgets = [
      SizedBox(height: 40),
      this.makeTitleEntryBox(controller),
      controller.model.type == RemarkTypes.text
          ? SizedBox(height: 0)
          : SizedBox(height: 40),
      this.getUploaderWidget(controller, context),
      SizedBox(height: 40),
      ...this.makeBodyEntryBox(controller, context),
      SizedBox(height: 40),
      ...this.makeTagCreator(controller, context),
      SizedBox(height: 40),
      this.makePrimaryButton(
        () => this.submitRemark(context, controller),
        "Submit Post",
      ),
      SizedBox(height: 40),
    ];

    var maxWidth = 600.0;
    var screenWidth = MediaQuery.of(context).size.width;
    var boxWidth = screenWidth <= maxWidth ? screenWidth - 20 : maxWidth;

    return ListView(
      children: [
        Center(
          child: SizedBox(
            width: boxWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: widgets,
            ),
          ),
        ),
      ],
    );
  }

  Widget makeTitleEntryBox(CreateRemarkController controller) {
    return TextField(
      decoration: InputDecoration(
        hintText: "Title Goes Here",
        border: OutlineInputBorder(),
      ),
      maxLength: 100,
      maxLengthEnforcement: MaxLengthEnforcement.enforced,
      controller: controller.titleController,
    );
  }

  List<Widget> makeBodyEntryBox(
      CreateRemarkController controller, BuildContext context) {
    List<Widget> widgets = [];

    widgets.add(
      Row(
        children: [
          IconButton(
            onPressed: () => controller.toggleLinkCreator(),
            icon: Icon(Icons.link),
          )
        ],
      ),
    );

    if (controller.model.showLinkCreator) {
      widgets.add(this.createLinkCreator(controller));
    }

    widgets.add(
      TextField(
        decoration: InputDecoration(
          hintText: "Body Goes Here",
          helperText: "Markdown supported, try googling it, its cool.",
          helperMaxLines: 2,
          border: OutlineInputBorder(),
        ),
        maxLength: 60000,
        maxLines: 15,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        controller: controller.bodyController,
      ),
    );

    return widgets;
  }

  Widget createLinkCreator(CreateRemarkController controller) {
    return Row(
      children: [
        Expanded(
          child: Card(
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                    hintText: "Link Display Text Here",
                    border: OutlineInputBorder(),
                  ),
                  controller: controller.linkTextController,
                ),
                TextField(
                  decoration: InputDecoration(
                    hintText: "Link Url Here",
                    border: OutlineInputBorder(),
                  ),
                  controller: controller.linkUrlController,
                ),
                this.makePrimaryButton(
                  () => controller.addLink(),
                  "Insert Link",
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget getUploaderWidget(
      CreateRemarkController controller, BuildContext context) {
    if (controller.model.type == RemarkTypes.text) {
      return Container();
    } else if (controller.model.type == RemarkTypes.images) {
      return this.makeImageUploader(controller, context);
    } else {
      return this.makeVideoUploader(controller, context);
    }
  }

  Widget makeImageUploader(
      CreateRemarkController controller, BuildContext context) {
    double maxCardWidth = 300.0;
    double screenWidth = MediaQuery.of(context).size.width;
    double cardWidth =
        maxCardWidth <= screenWidth / 2 ? maxCardWidth : screenWidth / 2;
    double cardHeight = 250.0;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Column(
            children: [
              this.makeImageDisplayStack(
                controller,
                0,
                cardWidth,
                cardHeight,
                context,
              ),
              this.makeImageDisplayStack(
                controller,
                2,
                cardWidth,
                cardHeight,
                context,
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              this.makeImageDisplayStack(
                controller,
                1,
                cardWidth,
                cardHeight,
                context,
              ),
              this.makeImageDisplayStack(
                controller,
                3,
                cardWidth,
                cardHeight,
                context,
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget makeImageDisplayStack(
    CreateRemarkController controller,
    int imageIndex,
    double width,
    double height,
    BuildContext context,
  ) {
    var imageWidget;
    var tileButton;
    var imageData = controller.model.images[imageIndex];
    if (imageData == null) {
      imageWidget = Positioned.fill(
        child: Card(),
      );
      tileButton = this.makeImageAddButton(controller, imageIndex, context);
    } else {
      imageWidget = Positioned.fill(
        child: Container(
          padding: EdgeInsets.all(2.0),
          child: Image.memory(
            controller.model.images[imageIndex]!.bytes!,
            fit: BoxFit.cover,
          ),
        ),
      );
      tileButton = this.makeImageRemoveButton(controller, imageIndex);
    }

    return SizedBox(
      height: height,
      width: width,
      child: Stack(
        alignment: Alignment.center,
        children: [
          imageWidget,
          tileButton,
        ],
      ),
    );
  }

  Widget makeImageAddButton(
    CreateRemarkController controller,
    int index,
    BuildContext context,
  ) {
    return FloatingActionButton(
      heroTag: "btn" + index.toString(),
      onPressed: () async {
        var image = await this.pickImage();
        if (image == null) {
          // Can show error to user here as well.
          return;
        } else {
          var message = await controller.addImageToModel(image, index);
          if (message != null) {
            Dialogs.basicDialog(message, context);
          }
        }
      },
      child: Icon(
        Icons.add_a_photo,
        color: Colors.white,
        size: 30,
      ),
    );
  }

  Widget makeImageRemoveButton(CreateRemarkController controller, int index) {
    return Positioned(
      right: 0,
      top: 0,
      child: SizedBox(
        height: 40,
        width: 40,
        child: FittedBox(
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                Colors.red,
              ),
              shape: MaterialStateProperty.all(
                CircleBorder(),
              ),
            ),
            onPressed: () {
              controller.removeImageFromModel(index);
            },
            child: Center(
              child: Icon(
                Icons.remove,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<XFile?> pickImage() async {
    var picker = new ImagePicker();
    var image = await picker.pickImage(source: ImageSource.gallery);
    return image;
  }

  Future<XFile?> pickVideo() async {
    var picker = new ImagePicker();
    var video = await picker.pickVideo(source: ImageSource.gallery);
    return video;
  }

  Widget makeVideoUploader(
    CreateRemarkController controller,
    BuildContext context,
  ) {
    return this.makePrimaryButton(() async {
      var videoFile = await this.pickVideo();
      var message = await controller.addVideoToModel(videoFile);
      if (message != null) {
        Dialogs.basicDialog(message, context);
      }
    }, "Select Video");
  }

  Widget makePrimaryButton(Function callback, String text) {
    return TextButton(
      onPressed: () => callback(),
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.resolveWith(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.blueGrey.shade300;
            } else {
              return Colors.blueGrey.shade400;
            }
          },
        ),
        backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
        fixedSize: MaterialStateProperty.all(Size(100, 50)),
      ),
    );
  }

  List<Widget> makeTagCreator(
    CreateRemarkController controller,
    BuildContext context,
  ) {
    List<Widget> widgets = [];

    widgets.addAll(
      [
        TagDisplay(
          tags: controller.model.tags,
          allowTagRemoval: true,
          removeTag: controller.removeTag,
        ),
        SizedBox(height: 50),
      ],
    );

    widgets.add(
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: TextField(
              controller: controller.addTagController,
              decoration: InputDecoration(
                helperText: "Separate tags with spaces.",
                hintText: "Tags Go Here",
                border: OutlineInputBorder(),
              ),
              maxLength: 500,
              maxLines: 1,
              maxLengthEnforcement: MaxLengthEnforcement.enforced,
            ),
          ),
          SizedBox(width: 20),
          this.makePrimaryButton(() => controller.addTag(), "Add Tag")
        ],
      ),
    );
    return widgets;
  }

  Future<void> submitRemark(
      BuildContext context, CreateRemarkController controller) async {
    bool success = await controller.submitRemark();
    if (!success) {
      Dialogs.basicDialog("Failed to submit post, try again later.", context);
    } else {
      Navigator.of(context).pop();
    }
  }
}

import 'dart:typed_data';
import 'package:cheapspeech_web/api/models/remark_model.dart';

class CreateRemarkModel {
  RemarkTypes type = RemarkTypes.unselected;
  List<UploadFile?> images = [null, null, null, null];
  UploadFile? video;
  List<String> tags = [];
  String? remarkId;
  String? parentId;
  bool showLinkCreator = false;
}

class UploadFile {
  String? name;
  Uint8List? bytes;
  UploadFile({this.name, this.bytes});
}

import 'package:cheapspeech_web/pages/homepage/homepage_controller.dart';
import 'package:cheapspeech_web/routes.dart';
import 'package:cheapspeech_web/widgets/remark_list.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return ChangeNotifierProvider(
      child: SharedScaffoldFactory.get(
        context: ctx,
        body: Consumer<HomePageController>(
          builder: (context, controller, child) {
            return RemarkList(
              remarkSelected: (data) {
                Navigator.of(ctx).pushNamed(
                  Routes.remark,
                  arguments: data,
                );
              },
              getRemarks: () {
                return controller.getRemarks();
              },
            );
          },
        ),
      ),
      create: (context) => HomePageController(),
    );
  }
}

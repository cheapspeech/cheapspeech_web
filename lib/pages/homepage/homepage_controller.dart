import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/api/providers/remark_provider.dart';
import 'package:cheapspeech_web/pages/homepage/homepage_model.dart';
import 'package:flutter/widgets.dart';

const int fetch_size = 20;

class HomePageController with ChangeNotifier {
  HomePageModel model = new HomePageModel();

  Future<List<RemarkModel>> getRemarks() {
    var start = this.model.remarkIndex;
    var length = fetch_size;
    this.model.remarkIndex = start + fetch_size;
    var provider = new RemarkProvider();
    return provider.getRemarks(start, length);
  }
}

import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/global_state/global_controller.dart';
import 'package:cheapspeech_web/pages/portfolio/profile_controller.dart';
import 'package:cheapspeech_web/routes.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';
import 'package:cheapspeech_web/widgets/remark_list.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:cheapspeech_web/widgets/tag_display.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var username = ModalRoute.of(context)!.settings.arguments;

    return ChangeNotifierProvider(
      create: (context) {
        var controller = new ProfileController();
        if (username is String) {
          controller.model.username = username;
          controller.model.usersOwnProfile = false;
        }
        controller.initializeModelFromApi();

        return controller;
      },
      child: SharedScaffoldFactory.get(
        body: Consumer<ProfileController>(
          builder: (context, controller, child) {
            if (!controller.model.modelFinishedInitializing) {
              return this.createLoadingCircle();
            } else if (controller.model.usersOwnProfile) {
              return this.createOwnProfileDisplay(context, controller);
            } else {
              return this.createOtherUsersProfileDisplay(context, controller);
            }
          },
        ),
        context: context,
        showBackArrow: true,
      ),
    );
  }

  Widget createOwnProfileDisplay(
    BuildContext context,
    ProfileController controller,
  ) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 40),
            Text(
              "Profile",
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
            SizedBox(height: 40),
            Text(
              "Follower Count: " + controller.model.followerCount.toString(),
            ),
            SizedBox(height: 40),
            this.createUserOwnedRemarksList(context),
            SizedBox(height: 40),
            this.createFollowingList(context),
            SizedBox(height: 40),
          ],
        ),
      ],
    );
  }

  Widget createOtherUsersProfileDisplay(
    BuildContext context,
    ProfileController controller,
  ) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 40),
            Text(
              "Profile",
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
            SizedBox(height: 40),
            Text(
              "Follower Count: " + controller.model.followerCount.toString(),
            ),
            SizedBox(height: 40),
            this.createFollowButton(),
            SizedBox(height: 40),
            this.createUserOwnedRemarksList(context),
            SizedBox(height: 40),
          ],
        ),
      ],
    );
  }

  Widget createLoadingCircle() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget createFollowButton() {
    return Consumer<ProfileController>(
      builder: (context, controller, child) {
        if (controller.model.usersOwnProfile) {
          return SizedBox(height: 0, width: 0);
        } else {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  var globalController =
                      Provider.of<GlobalController>(context, listen: false);
                  if (!globalController.model.isLoggedIn) {
                    Dialogs.basicDialog(
                      "Please login to follow / unfollow users.",
                      context,
                    );
                  } else if (controller.model.isFollowingUser) {
                    controller.unfollowUser();
                  } else {
                    controller.followUser();
                  }
                },
                child: Text(
                  controller.model.isFollowingUser ? "Unfollow" : "Follow",
                  style: TextStyle(color: Colors.white),
                ),
                style: ButtonStyle(
                  overlayColor: MaterialStateProperty.resolveWith(
                    (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) {
                        return Colors.blueGrey.shade300;
                      } else {
                        return Colors.blueGrey.shade400;
                      }
                    },
                  ),
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                  fixedSize: MaterialStateProperty.all(Size(100, 50)),
                ),
              ),
            ],
          );
        }
      },
    );
  }

  Widget createUserOwnedRemarksList(BuildContext context) {
    return Consumer<ProfileController>(
      builder: (context, controller, child) {
        return Container(
          height: 600,
          width: 400,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.circular(5),
          ),
          child: RemarkList(
            body: Padding(
              padding: EdgeInsets.only(left: 10, top: 10),
              child: Text(
                "Created Remarks",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  fontSize: 20,
                ),
              ),
            ),
            getRemarks: controller.getRemarks,
            remarkSelected: (model) => this.remarkSelected(
              model,
              context,
              controller,
            ),
            maxTileWidth: 350,
          ),
        );
      },
    );
  }

  void remarkSelected(
    RemarkModel model,
    BuildContext context,
    ProfileController controller,
  ) async {
    if (controller.model.usersOwnProfile) {
      var option = await Dialogs.optionDialog(
        title: "What Do?",
        message: "Pick one...",
        context: context,
        options: ["delete", "view"],
      );
      if (option == "view") {
        Navigator.of(context).pushNamed(
          Routes.remark,
          arguments: model,
        );
      } else if (option == "delete") {
        controller.deleteRemark(model);
        Dialogs.basicDialog(
          "Your remark is in the process of being deleted.",
          context,
        );
      }
    } else {
      Navigator.of(context).pushNamed(
        Routes.remark,
        arguments: model,
      );
    }
  }

  Widget createFollowingList(BuildContext context) {
    return Consumer<ProfileController>(
      builder: (context, controller, child) {
        return Container(
          height: 600,
          width: 400,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.circular(5),
          ),
          child: ListView(
            scrollDirection: Axis.vertical,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10, left: 10),
                child: Text(
                  "Followed Users",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 20,
                  ),
                ),
              ),
              TagDisplay(
                tags: controller.model.followedUsers,
                allowTagRemoval: false,
                onTagTap: (username) {
                  Navigator.of(context).pushNamed(
                    Routes.profile,
                    arguments: username,
                  );
                },
                displayHashTags: false,
              ),
            ],
          ),
        );
      },
    );
  }
}

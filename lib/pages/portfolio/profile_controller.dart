import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/api/providers/follower_provider.dart';
import 'package:cheapspeech_web/api/providers/remark_provider.dart';
import 'package:cheapspeech_web/pages/portfolio/profile_model.dart';
import 'package:flutter/widgets.dart';

const int fetch_size = 20;

class ProfileController with ChangeNotifier {
  ProfileModel model = new ProfileModel();

  deleteRemark(RemarkModel model) async {
    var provider = new RemarkProvider();
    await provider.deleteRemark(model.remarkId ?? "");
  }

  Future<List<RemarkModel>> getRemarks() async {
    if (this.model.recievedLastReply) {
      return [];
    } else {
      var provider = new RemarkProvider();
      var remarks = await provider.getUserOwnedRemarks(
        fetch_size,
        (String pageState) => this.model.pageState = pageState,
        () => this.model.recievedLastReply = true,
        pageState: this.model.pageState,
        username: this.model.username,
      );
      return remarks;
    }
  }

  Future<void> initializeModelFromApi() async {
    FollowerProvider provider = new FollowerProvider();
    List<Future<void>> calls = [
      this.getFollowerCount(provider),
    ];
    if (!this.model.usersOwnProfile) {
      calls.add(this.getIsFollowingUser(provider));
    } else {
      calls.add(this.getFollowedUsers(provider));
    }
    await Future.wait(calls);

    this.model.modelFinishedInitializing = true;
    notifyListeners();
  }

  Future<void> getFollowerCount(FollowerProvider provider) async {
    var data = await provider.getFollowerCount(this.model.username);
    this.model.followerCount = data.followerCount ?? 0;
  }

  Future<void> getFollowedUsers(FollowerProvider provider) async {
    var data = await provider.getFollowedUsers();
    this.model.followedUsers = data.following ?? [];
  }

  Future<void> getIsFollowingUser(FollowerProvider provider) async {
    var data = await provider.getIsFollowingUser(this.model.username);
    this.model.isFollowingUser = data.isFollowing ?? false;
  }

  Future<void> followUser() async {
    if (this.model.usersOwnProfile) {
      return;
    } else {
      var provider = new FollowerProvider();
      var success = await provider.followUser(this.model.username);
      if (success) {
        this.model.isFollowingUser = true;
        notifyListeners();
      }
    }
  }

  Future<void> unfollowUser() async {
    if (this.model.usersOwnProfile) {
      return;
    } else {
      var provider = new FollowerProvider();
      var success = await provider.unfollowUser(this.model.username);
      if (success) {
        this.model.isFollowingUser = false;
        notifyListeners();
      }
    }
  }
}

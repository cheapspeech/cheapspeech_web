import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/api/models/vote_model.dart';
import 'package:cheapspeech_web/api/providers/remark_provider.dart';
import 'package:cheapspeech_web/api/providers/vote_provider.dart';
import 'package:cheapspeech_web/global_state/global_controller.dart';
import 'package:cheapspeech_web/pages/view_remark/view_remark_model.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

const int fetch_size = 20;

class ViewRemarkController with ChangeNotifier {
  late VideoPlayerController videoPlayerController;
  late ChewieController chewieController;
  ViewRemarkModel model = new ViewRemarkModel();
  RemarkModel apiData;

  ViewRemarkController({required this.apiData});

  Future<void> initializeVideoControllers() async {
    if (this.model.videoUrl == null) {
      this.model.hasPlayableVideo = false;
      this.model.videoPlayerInitComplete = true;
      return;
    }

    this.videoPlayerController = new VideoPlayerController.network(
      this.model.videoUrl!,
    );

    await this.videoPlayerController.initialize();
    this.chewieController = new ChewieController(
      videoPlayerController: videoPlayerController,
      allowFullScreen: false,
    );

    this.model.hasPlayableVideo = true;
    this.model.videoPlayerInitComplete = true;

    notifyListeners();
  }

  void changeSelectedImage(int spaces) {
    if (this.apiData.images == null) {
      return;
    }
    var totalImages = this.apiData.images!.length;
    var nextSpace =
        (spaces % totalImages + this.model.onDeckImage) % totalImages;
    this.model.onDeckImage = nextSpace;
    notifyListeners();
  }

  Future<List<RemarkModel>> getRemarks() async {
    if (this.model.recievedLastReply) {
      return [];
    } else {
      var provider = new RemarkProvider();
      var remarks = await provider.getReplies(
        fetch_size,
        this.model.remarkId ?? "",
        this.model.pageState,
        (String pageState) => this.model.pageState = pageState,
        () => this.model.recievedLastReply = true,
      );
      return remarks;
    }
  }

  Future<RemarkModel> getParentRemark() async {
    var provider = new RemarkProvider();
    var result = await provider.getSingleRemark(this.model.parentId!);
    return result;
  }

  Future<void> upvote(BuildContext context) async {
    var globalController = Provider.of<GlobalController>(
      context,
      listen: false,
    );

    if (!globalController.model.isLoggedIn) {
      Dialogs.basicDialog("Please login to vote.", context);
    } else if (this.model.hasUpvoted) {
      await this._removeVote();
    } else {
      this.model.hasDownvoted = false;
      this.model.hasUpvoted = true;
      var provider = new VoteProvider();
      var response = await provider.voteOnPost(
        this.model.remarkId ?? "",
        VoteTypes.upvote,
      );

      this.model.downvotes = response.downvotes ?? this.model.downvotes;
      this.model.upvotes = response.upvotes ?? this.model.upvotes;
    }
    notifyListeners();
  }

  Future<void> downvote(BuildContext context) async {
    var globalController = Provider.of<GlobalController>(
      context,
      listen: false,
    );

    if (!globalController.model.isLoggedIn) {
      Dialogs.basicDialog("Please login to vote.", context);
    } else if (this.model.hasDownvoted) {
      await this._removeVote();
    } else {
      this.model.hasDownvoted = true;
      this.model.hasUpvoted = false;
      var provider = new VoteProvider();
      var response = await provider.voteOnPost(
        this.model.remarkId ?? "",
        VoteTypes.downvote,
      );

      this.model.downvotes = response.downvotes ?? this.model.downvotes;
      this.model.upvotes = response.upvotes ?? this.model.upvotes;
    }
    notifyListeners();
  }

  Future<void> _removeVote() async {
    this.model.hasDownvoted = false;
    this.model.hasUpvoted = false;
    var provider = new VoteProvider();
    var response = await provider.voteOnPost(
      this.model.remarkId ?? "",
      VoteTypes.none,
    );

    this.model.downvotes = response.downvotes ?? this.model.downvotes;
    this.model.upvotes = response.upvotes ?? this.model.upvotes;
  }

  Future<void> retrieveUserVoteStatus(BuildContext context) async {
    var globalController = Provider.of<GlobalController>(
      context,
      listen: false,
    );

    if (!globalController.model.isLoggedIn) {
      this.model.hasDownvoted = false;
      this.model.hasUpvoted = false;
      return;
    }

    var provider = new VoteProvider();
    var response = await provider.getHasVotedOnPost(this.model.remarkId ?? "");
    if (response.type == VoteTypes.upvote) {
      this.model.hasDownvoted = false;
      this.model.hasUpvoted = true;
    } else if (response.type == VoteTypes.downvote) {
      this.model.hasDownvoted = true;
      this.model.hasUpvoted = false;
    } else {
      this.model.hasDownvoted = false;
      this.model.hasUpvoted = false;
    }

    this.model.hasRetrievedUserVoteStatus = true;
    notifyListeners();
  }
}

class ViewRemarkModel {
  int onDeckImage = 0;
  String? remarkId = "";
  String? videoUrl;
  String pageState = "";
  bool recievedLastReply = false;
  bool videoPlayerInitComplete = false;
  bool hasPlayableVideo = false;
  bool hasUpvoted = false;
  bool hasDownvoted = false;
  int? upvotes = 0;
  int? downvotes = 0;
  String? parentId;
  bool hasRetrievedUserVoteStatus = false;
}

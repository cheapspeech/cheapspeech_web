import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/global_state/global_controller.dart';
import 'package:cheapspeech_web/pages/view_remark/view_remark_controller.dart';
import 'package:cheapspeech_web/routes.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';
import 'package:cheapspeech_web/widgets/remark_list.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:cheapspeech_web/widgets/tag_display.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewRemark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var remarkData = ModalRoute.of(context)!.settings.arguments;
    if (!(remarkData is RemarkModel)) {
      print(
          "ViewRemark widget must recieve route arguments of type RemarkModel!! Popping route.");
      Navigator.of(context).maybePop();
      return Container();
    }
    return ChangeNotifierProvider(
      child: SharedScaffoldFactory.get(
        context: context,
        body: this.buildPost(remarkData, context),
        showBackArrow: true,
      ),
      create: (context) {
        var controller = ViewRemarkController(apiData: remarkData);
        controller.model.remarkId = remarkData.remarkId;
        controller.model.upvotes = remarkData.upvotes;
        controller.model.downvotes = remarkData.downvotes;
        controller.model.parentId = remarkData.parentId;
        if (remarkData.video == null || remarkData.video!.length < 1) {
          controller.model.videoUrl = null;
        } else {
          controller.model.videoUrl = remarkData.video![0];
        }
        return controller;
      },
    );
  }

  Widget buildPost(RemarkModel data, context) {
    List<Widget> widgets = [
      SizedBox(height: 50),
      this.makeSelectableText(
        context,
        data.title,
        weight: FontWeight.bold,
        size: 40,
        decoration: TextDecoration.underline,
      ),
      SizedBox(height: 50),
      this.makeClickableUserPlug(context, data.owner),
      SizedBox(height: 50),
      TagDisplay(tags: data.tags ?? [], allowTagRemoval: false),
      SizedBox(height: 50),
    ];
    if (data.type == RemarkTypes.images && (data.images?.length ?? 0) > 0) {
      widgets.addAll(
        [
          this.makeImageGallery(context, data.images),
          SizedBox(height: 50),
        ],
      );
    } else if (data.type == RemarkTypes.videos) {
      widgets.addAll([
        this.makeVideoPlayer(context),
        SizedBox(height: 50),
      ]);
    }
    widgets.addAll(
      [
        this.makeUpvoteDownvoteSection(context),
        SizedBox(height: 50),
        this.makeMarkdownText(
          context,
          data.text,
        ),
        SizedBox(height: 50),
      ],
    );
    var postDisplay = Column(
      children: widgets,
    );

    return this.makeCommentsSection(context, postDisplay);
  }

  Widget makeClickableUserPlug(BuildContext context, String? owner) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Created by: "),
        InkWell(
          onTap: () => this.navigateToCreatorProfile(context, owner),
          child: Card(
            semanticContainer: true,
            child: Padding(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    owner ?? "Not Found",
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
              padding: EdgeInsets.all(5),
            ),
          ),
        ),
      ],
    );
  }

  Widget makeUpvoteDownvoteSection(BuildContext context) {
    return Consumer<ViewRemarkController>(
      builder: (context, controller, child) {
        if (!controller.model.hasRetrievedUserVoteStatus) {
          controller.retrieveUserVoteStatus(context);
        }

        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              tooltip: "View Parent Remark",
              onPressed: () async {
                if (controller.model.parentId != null) {
                  var model = await controller.getParentRemark();
                  Navigator.of(context).pushNamed(
                    Routes.remark,
                    arguments: model,
                  );
                } else {
                  Dialogs.basicDialog(
                    "Could not locate parent remark.",
                    context,
                  );
                }
              },
              icon: Icon(
                Icons.arrow_upward,
              ),
            ),
            SizedBox(width: 50),
            Column(
              children: [
                IconButton(
                  onPressed: () => controller.upvote(context),
                  icon: Icon(
                    Icons.thumb_up,
                    color: controller.model.hasUpvoted
                        ? Colors.blueGrey
                        : Colors.black,
                  ),
                ),
                Text((controller.model.upvotes ?? 0).toString()),
              ],
            ),
            SizedBox(width: 50),
            Column(
              children: [
                IconButton(
                  onPressed: () => controller.downvote(context),
                  icon: Icon(
                    Icons.thumb_down,
                    color: controller.model.hasDownvoted
                        ? Colors.blueGrey
                        : Colors.black,
                  ),
                ),
                Text((controller.model.downvotes ?? 0).toString()),
              ],
            ),
            SizedBox(width: 50),
            IconButton(
              onPressed: () {
                var loggedIn =
                    Provider.of<GlobalController>(context, listen: false)
                        .model
                        .isLoggedIn;
                if (!loggedIn) {
                  Dialogs.basicDialog("Please login to reply.", context);
                } else {
                  Navigator.of(context).pushNamed(
                    Routes.create,
                    arguments: controller.model.remarkId,
                  );
                }
              },
              icon: Icon(
                Icons.reply,
              ),
            ),
          ],
        );
      },
    );
  }

  Widget makeImageGallery(BuildContext ctx, List<String>? images) {
    var screenWidth = MediaQuery.of(ctx).size.width;
    var imageWidth = screenWidth;
    var imageHeight = imageWidth > 400 ? 400.0 : imageWidth;
    return Consumer<ViewRemarkController>(
      builder: (context, controller, child) {
        return Container(
          width: screenWidth < 500 ? screenWidth : 500,
          child: Stack(
            alignment: Alignment.center,
            children: [
              CachedNetworkImage(
                fit: BoxFit.contain,
                height: imageHeight,
                width: imageWidth,
                imageUrl: images?[controller.model.onDeckImage] ?? "",
                placeholder: (context, url) => Center(
                  child: SizedBox(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              this.makeImageGalleryButton(
                icon: Icons.arrow_forward,
                inset: EdgeInsets.fromLTRB(10, 0, 0, 0),
                align: Alignment.centerRight,
                onPressed: () {
                  controller.changeSelectedImage(1);
                },
              ),
              this.makeImageGalleryButton(
                icon: Icons.arrow_back,
                inset: EdgeInsets.fromLTRB(0, 0, 10, 0),
                align: Alignment.centerLeft,
                onPressed: () {
                  controller.changeSelectedImage(-1);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Widget makeImageGalleryButton({
    required IconData icon,
    required Alignment align,
    required EdgeInsets inset,
    required Function onPressed,
  }) {
    return Padding(
      padding: inset,
      child: Align(
        alignment: align,
        child: TextButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                Colors.blueGrey.withOpacity(0.5),
              ),
              shape: MaterialStateProperty.all(
                CircleBorder(),
              ),
              fixedSize: MaterialStateProperty.all(Size(40, 40))),
          onPressed: () => onPressed(),
          child: Center(
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Widget makeVideoPlayer(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var playerWidth = screenWidth > 600 ? 600.0 : screenWidth - 10;
    var playerHeight = (3 / 4) * playerWidth;

    return Consumer<ViewRemarkController>(
      builder: (context, controller, child) {
        if (!controller.model.videoPlayerInitComplete) {
          controller.initializeVideoControllers();
          return Center(child: CircularProgressIndicator());
        } else if (!controller.model.hasPlayableVideo) {
          return Text("Video is not playable.");
        } else {
          return SizedBox(
            width: playerWidth,
            height: playerHeight,
            child: Chewie(
              controller: controller.chewieController,
            ),
          );
        }
      },
    );
  }

  Widget makeSelectableText(
    BuildContext ctx,
    String? text, {
    TextDecoration decoration = TextDecoration.none,
    FontWeight weight = FontWeight.normal,
    double size = 14,
  }) {
    var screenWidth = MediaQuery.of(ctx).size.width;
    var textWidth = screenWidth > 600 ? 600.0 : screenWidth - 20;
    return SizedBox(
      width: textWidth,
      child: SelectableText(
        text ?? "Deleted",
        textAlign: TextAlign.center,
        style: TextStyle(
          decoration: decoration,
          fontWeight: weight,
          fontSize: size,
        ),
      ),
    );
  }

  Widget makeMarkdownText(BuildContext ctx, String? text) {
    var screenWidth = MediaQuery.of(ctx).size.width;
    var textWidth = screenWidth > 600 ? 600.0 : screenWidth - 20;
    return SizedBox(
      width: textWidth,
      child: MarkdownBody(
        data: text ?? "Deleted",
        selectable: true,
        onTapLink: (text, href, title) async {
          if (href != null && await canLaunch(href)) {
            await launch(href);
          } else {
            Dialogs.basicDialog(
              "Could not launch link at $href",
              ctx,
            );
          }
        },
      ),
    );
  }

  Widget makeCommentsSection(BuildContext ctx, Widget body) {
    var screenWidth = MediaQuery.of(ctx).size.width;
    var tileWidth = screenWidth > 800 ? 800.0 : screenWidth - 20;

    return Consumer<ViewRemarkController>(
        builder: (context, controller, child) {
      return RemarkList(
        remarkSelected: (data) {
          Navigator.of(ctx).pushNamed(
            Routes.remark,
            arguments: data,
          );
        },
        getRemarks: () {
          return controller.getRemarks();
        },
        maxTileWidth: tileWidth,
        body: body,
      );
    });
  }

  void navigateToCreatorProfile(BuildContext context, String? ownerName) {
    if (ownerName == null) {
      Dialogs.basicDialog("Failed to locate user.", context);
    } else {
      Navigator.of(context).pushNamed(Routes.profile, arguments: ownerName);
    }
  }
}

import 'package:cheapspeech_web/global_state/global_controller.dart';
import 'package:cheapspeech_web/pages/login/login_controller.dart';
import 'package:cheapspeech_web/pages/login/login_model.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Login extends StatelessWidget {
  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController recoveryController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    var task = ModalRoute.of(context)!.settings.arguments;
    if (!(task is LoginPageTasks)) {
      print(
          "ViewRemark widget must recieve route arguments of type LoginPageTasks!! Popping route.");
      Navigator.of(context).maybePop();
      return Container();
    }
    var maxWidth = 300.0;
    var screenWidth = MediaQuery.of(context).size.width;
    var boxWidth = screenWidth <= maxWidth ? screenWidth - 20 : maxWidth;

    return ChangeNotifierProvider(
      create: (context) => LoginController(task),
      child: SharedScaffoldFactory.get(
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: boxWidth,
              child: Consumer<LoginController>(
                builder: (context, controller, child) {
                  return this.getPageContent(controller, context);
                },
              ),
            ),
          ],
        ),
        context: context,
        showBackArrow: true,
      ),
    );
  }

  Widget getPageContent(LoginController controller, BuildContext context) {
    if (controller.model.showLoader) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Cheap Speech",
            style: TextStyle(
              fontSize: 40,
            ),
          ),
          SizedBox(height: 20),
          ...this.getEntryBoxes(controller),
          SizedBox(height: 20),
          ...this.getButtons(controller, context),
        ],
      );
    }
  }

  List<Widget> getEntryBoxes(LoginController controller) {
    var passwordBox = TextField(
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Password",
        hintText: controller.model.task == LoginPageTasks.signup
            ? "Pick a good one!"
            : "",
      ),
      controller: this.passwordController,
    );
    var usernameBox = TextField(
      obscureText: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Username",
        hintText: controller.model.task == LoginPageTasks.signup
            ? "Must be unique."
            : "",
      ),
      controller: this.usernameController,
    );
    return [
      usernameBox,
      SizedBox(height: 20),
      passwordBox,
    ];
  }

  List<Widget> getButtons(LoginController controller, BuildContext context) {
    if (controller.model.task == LoginPageTasks.login) {
      return [
        this.makeMainButton(
            () => this.submitLoginInformation(context, controller), "Login"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.signup);
        }, "Sign Up"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.recoverAccount);
        }, "Recover Account"),
      ];
    } else if (controller.model.task == LoginPageTasks.signup) {
      return [
        this.makeMainButton(
            () => this.submitRegistrationInformation(context, controller),
            "Sign Up"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.login);
        }, "Login"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.recoverAccount);
        }, "Recover Account"),
      ];
    } else {
      return [
        this.makeMainButton(
            () => this.submitRecoveryInformation(context), "Recover"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.login);
        }, "Login"),
        this.makeSecondaryButton(() {
          controller.switchSelectedTask(LoginPageTasks.signup);
        }, "Sign Up"),
      ];
    }
  }

  Widget makeMainButton(dynamic onPressed, String text) {
    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.resolveWith(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.blueGrey.shade300;
            } else {
              return Colors.blueGrey.shade400;
            }
          },
        ),
        backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
        fixedSize: MaterialStateProperty.all(
          Size(100, 40),
        ),
      ),
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget makeSecondaryButton(Function onPressed, String text) {
    return TextButton(
      onPressed: () => onPressed(),
      child: Text(
        text,
        style: TextStyle(color: Colors.blue, fontSize: 15),
      ),
    );
  }

  Future<void> submitLoginInformation(
    BuildContext ctx,
    LoginController controller,
  ) async {
    controller.toggleLoader(true);
    String username = this.usernameController.text;
    String password = this.passwordController.text;
    var globalController = Provider.of<GlobalController>(ctx, listen: false);
    var successful = await globalController.login(username, password);
    if (successful) {
      Navigator.of(ctx).pop();
    } else {
      Dialogs.basicDialog("Incorrect username / password combo.", ctx);
    }
    controller.toggleLoader(false);
  }

  void submitRegistrationInformation(
    BuildContext ctx,
    LoginController controller,
  ) async {
    controller.toggleLoader(true);
    String username = this.usernameController.text;
    String password = this.passwordController.text;
    var globalController = Provider.of<GlobalController>(ctx, listen: false);
    var successful = await globalController.createAccount(username, password);
    if (successful) {
      Navigator.of(ctx).pop();
    } else {
      Dialogs.basicDialog(
        "Failed to create user account, try another username.",
        ctx,
      );
    }

    controller.toggleLoader(false);
  }

  void submitRecoveryInformation(BuildContext ctx) {}
}

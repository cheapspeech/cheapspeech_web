import 'package:flutter/widgets.dart';

import 'login_model.dart';

class LoginController with ChangeNotifier {
  LoginModel model = new LoginModel();

  LoginController(LoginPageTasks? task) {
    this.model.task = task ?? LoginPageTasks.login;
  }

  void switchSelectedTask(LoginPageTasks task) {
    this.model.task = task;
    notifyListeners();
  }

  void toggleLoader(bool show) {
    this.model.showLoader = show;
    notifyListeners();
  }
}

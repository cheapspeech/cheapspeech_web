import 'package:cheapspeech_web/routes.dart';
import 'package:cheapspeech_web/widgets/shared_scaffold.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  final TextStyle bodyStyle = new TextStyle(fontSize: 17);
  final TextStyle subHeadingStyle = new TextStyle(
    fontSize: 20,
    decoration: TextDecoration.underline,
  );

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var textMaxWidth = screenWidth > 710 ? 700.0 : screenWidth - 10.0;
    return SharedScaffoldFactory.get(
      body: Center(
      ),
      context: context,
    );
  }
}

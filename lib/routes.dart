import 'package:cheapspeech_web/pages/create_remark/create_remark.dart';
import 'package:cheapspeech_web/pages/homepage/homepage.dart';
import 'package:cheapspeech_web/pages/login/login.dart';
import 'package:cheapspeech_web/pages/portfolio/profile.dart';
import 'package:cheapspeech_web/pages/static_pages/welcome_page.dart';
import 'package:cheapspeech_web/pages/view_remark/view_remark.dart';
import 'package:flutter/material.dart';

typedef RouteBuilder = Widget Function(BuildContext);

class Routes {
  static String home = "/";
  static String remark = "/remark";
  static String create = "/create";
  static String login = "/login";
  static String profile = "/profile";
  static String welcome = "/welcome";

  static Map<String, RouteBuilder> routes = {
    Routes.home: (context) => HomePage(),
    Routes.remark: (context) => ViewRemark(),
    Routes.create: (context) => CreateRemark(),
    Routes.login: (context) => Login(),
    Routes.profile: (context) => Profile(),
    Routes.welcome: (context) => WelcomePage(),
  };
}

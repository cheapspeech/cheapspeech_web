import 'dart:convert';

class UserModel {
  String? username;
  String? password;

  UserModel();

  UserModel.fromJson(String jsonInput) {
    var json = jsonDecode(jsonInput);
    this.username = json["username"];
    this.password = json["password"];
  }

  String toJson() {
    return jsonEncode({
      "username": this.username,
      "password": this.password,
    });
  }
}

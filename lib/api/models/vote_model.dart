import 'dart:convert';

class VoteModel {
  int? upvotes;
  int? downvotes;
  bool? hasVoted;
  VoteTypes? type;
  String? remarkId;

  VoteModel() {
    this.upvotes = 0;
    this.downvotes = 0;
    this.hasVoted = false;
    this.type = VoteTypes.none;
    this.remarkId = "";
  }

  VoteModel.fromJson(String jsonInput) {
    Map<String, dynamic> json = jsonDecode(jsonInput);
    this.upvotes = json["upvotes"];
    this.downvotes = json["downvotes"];
    this.hasVoted = json["hasVoted"];
    this.type = this.getTypeFromString(json["type"]);
    this.remarkId = json["remarkId"];
  }

  VoteTypes getTypeFromString(String? input) {
    if (input == null || input.isEmpty) {
      return VoteTypes.none;
    } else if (input == "upvote") {
      return VoteTypes.upvote;
    } else if (input == "downvote") {
      return VoteTypes.downvote;
    } else {
      return VoteTypes.none;
    }
  }

  String getStringFromType(VoteTypes? type) {
    if (type == null || type == VoteTypes.none) {
      return "";
    } else if (type == VoteTypes.downvote) {
      return "downvote";
    } else if (type == VoteTypes.upvote) {
      return "upvote";
    } else {
      return "";
    }
  }

  String toJson() {
    return jsonEncode({
      "upvotes": this.upvotes,
      "downvotes": this.downvotes,
      "hasVoted": this.hasVoted,
      "type": this.getStringFromType(this.type),
      "remarkId": this.remarkId,
    });
  }
}

enum VoteTypes { upvote, downvote, none }

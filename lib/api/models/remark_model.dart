import 'dart:convert';

class RemarkModel {
  String? text;
  List<String>? tags;
  String? title;
  List<String>? images;
  List<String>? video;
  RemarkTypes? type;
  String? remarkId;
  String? parentId;
  int? upvotes;
  int? downvotes;
  String? owner;

  RemarkModel();

  RemarkModel.fromJson(String jsonInput) {
    Map<String, dynamic> json = jsonDecode(jsonInput);
    this.text = json["text"];
    this.title = json["title"];
    this.type = this.getType(json["type"]);
    this.images = json.containsKey("images")
        ? new List<String>.from(json["images"])
        : null;
    this.video =
        json.containsKey("video") ? new List<String>.from(json["video"]) : null;
    this.tags =
        json.containsKey("tags") ? new List<String>.from(json["tags"]) : null;
    this.remarkId = json["remarkId"];
    this.parentId = json["parentId"];
    this.upvotes = json["upvotes"];
    this.downvotes = json["downvotes"];
    this.owner = json["owner"];
  }

  RemarkTypes getType(String? type) {
    if (type == "images") {
      return RemarkTypes.images;
    } else if (type == "video") {
      return RemarkTypes.videos;
    } else {
      return RemarkTypes.text;
    }
  }

  String getTextType(RemarkTypes? type) {
    if (type == RemarkTypes.images) {
      return "images";
    } else if (type == RemarkTypes.videos) {
      return "video";
    } else {
      return "text";
    }
  }

  String toJson() {
    return jsonEncode({
      "text": this.text,
      "title": this.title,
      "images": this.images,
      "video": this.video,
      "type": this.getTextType(this.type),
      "tags": this.tags,
      "remarkId": this.remarkId,
      "parentId": this.parentId,
      "upvotes": this.upvotes,
      "downvotes": this.downvotes,
      "owner": this.owner,
    });
  }
}

enum RemarkTypes { images, videos, text, unselected }

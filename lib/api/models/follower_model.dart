import 'dart:convert';

class FollowerModel {
  int? followerCount;
  List<String>? following;
  bool? isFollowing;

  FollowerModel() {
    this.followerCount = 0;
    this.following = [];
    this.isFollowing = false;
  }

  FollowerModel.fromJson(String jsonInput) {
    Map<String, dynamic> json = jsonDecode(jsonInput);
    this.followerCount = json["followerCount"];
    this.following = json.containsKey("following")
        ? List<String>.from(json["following"])
        : null;
    this.isFollowing = json["isFollowing"];
  }

  String toJson() {
    return jsonEncode({
      "followerCount": this.followerCount,
      "following": this.following,
      "isFollowing": this.isFollowing
    });
  }
}

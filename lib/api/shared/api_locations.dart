class ApiLoc {
  // Test
  // static String protocol = "http";
  // static String host = "localhost";
  // static int port = 8000;

  // Prod
  static String protocol = "https";
  static String host = "cheapspeech.io";
  static int port = 443;

  static Uri getUri({
    required List<String> segments,
    Map<String, String> qp: const {},
  }) {
    var segmentsSwap = ["api"];
    segmentsSwap.addAll(segments);
    segments = segmentsSwap;

    if (qp.isEmpty) {
      return new Uri(
        scheme: ApiLoc.protocol,
        host: ApiLoc.host,
        port: ApiLoc.port,
        pathSegments: segments,
      );
    } else {
      return new Uri(
        scheme: ApiLoc.protocol,
        host: ApiLoc.host,
        port: ApiLoc.port,
        pathSegments: segments,
        queryParameters: qp,
      );
    }
  }
}

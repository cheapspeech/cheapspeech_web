import 'dart:convert';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utilities {
  static Future<String> getSessionToken() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getString("sessionToken") ?? "";
  }

  static Future<void> setSessionToken(Response response) async {
    var sessionToken = response.headers["session-token"];
    if (sessionToken != null) {
      var prefs = await SharedPreferences.getInstance();
      await prefs.setString("sessionToken", sessionToken);
    }
  }

  static Future<void> deleteSessionToken() async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.remove("sessionToken");
  }

  static dynamic nullSafeDecode(String? json) {
    if (json == null) {
      return null;
    } else {
      return jsonDecode(json);
    }
  }
}

import 'package:cheapspeech_web/api/shared/utilities.dart';
import 'package:http/http.dart' as http;
import 'package:cheapspeech_web/api/shared/api_locations.dart';
import 'package:cheapspeech_web/api/models/user_model.dart';

class UserProvider {
  Future<bool> login(String username, String password) async {
    var user = new UserModel();
    user.password = password;
    user.username = username;
    try {
      var response = await http.post(
        ApiLoc.getUri(segments: ["user", "login"]),
        body: user.toJson(),
        headers: {
          "content-type": "application/json",
        },
      );
      var sessionToken = response.headers["session-token"];

      if (sessionToken == null) {
        return false;
      } else {
        Utilities.setSessionToken(response);
        return response.statusCode == 200;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> logout() async {
    try {
      var sessionToken = await Utilities.getSessionToken();
      await http.post(
        ApiLoc.getUri(segments: ["user", "logout"]),
        headers: {
          "session-token": sessionToken,
        },
      );
      await Utilities.deleteSessionToken();
    } catch (e) {
      print(e);
    }
  }

  Future<bool> createAccount(String username, String password) async {
    var user = new UserModel();
    user.password = password;
    user.username = username;

    try {
      var response = await http.post(
        ApiLoc.getUri(segments: ["user"]),
        body: user.toJson(),
        headers: {"content-type": "application/json"},
      );

      Utilities.setSessionToken(response);

      return response.statusCode == 200;
    } catch (e) {
      print(e);
      return false;
    }
  }
}

import 'package:cheapspeech_web/api/models/vote_model.dart';
import 'package:cheapspeech_web/api/shared/api_locations.dart';
import 'package:cheapspeech_web/api/shared/utilities.dart';
import 'package:http/http.dart' as http;

class VoteProvider {
  Future<VoteModel> voteOnPost(String remarkId, VoteTypes type) async {
    var uri = ApiLoc.getUri(segments: ["votes"]);
    var body = VoteModel();
    body.type = type;
    body.remarkId = remarkId;

    var response = await http.post(
      uri,
      headers: {
        "content-type": "application/json",
        "session-token": await Utilities.getSessionToken()
      },
      body: body.toJson(),
    );

    var responseDecoded = VoteModel.fromJson(response.body);
    return responseDecoded;
  }

  Future<VoteModel> getHasVotedOnPost(String remarkId) async {
    var uri = ApiLoc.getUri(
      segments: ["votes", "user"],
      qp: {
        "remarkId": remarkId,
      },
    );

    var response = await http.get(
      uri,
      headers: {
        "content-type": "application/json",
        "session-token": await Utilities.getSessionToken()
      },
    );

    var responseDecoded = VoteModel.fromJson(response.body);
    return responseDecoded;
  }
}

import 'dart:convert';
import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/api/shared/api_locations.dart';
import 'package:cheapspeech_web/api/shared/utilities.dart';
import 'package:cheapspeech_web/pages/create_remark/create_remark_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class RemarkProvider {
  Future<List<RemarkModel>> getRemarks(int start, int length) async {
    var nextRemarksUri = ApiLoc.getUri(
      segments: ["trending"],
      qp: {
        "start": start.toString(),
        "length": length.toString(),
      },
    );
    var response = await http.get(nextRemarksUri);
    List<String> nextRemarks = new List<String>.from(jsonDecode(response.body));
    return await this.getRemarksFromIds(nextRemarks);
  }

  Future<List<RemarkModel>> getReplies(
    int length,
    String remarkId,
    String pageState,
    Function setPageState,
    Function setGotLastReply,
  ) async {
    Uri repliesUri;
    if (pageState.isNotEmpty) {
      repliesUri = ApiLoc.getUri(
        segments: ["replies"],
        qp: {
          "length": length.toString(),
          "remarkId": remarkId,
          "pageState": pageState,
        },
      );
    } else {
      repliesUri = ApiLoc.getUri(
        segments: ["replies"],
        qp: {
          "length": length.toString(),
          "remarkId": remarkId,
        },
      );
    }

    var response = await http.get(repliesUri);
    Map<String, dynamic> replies = jsonDecode(response.body);
    if (replies["pageState"] != null) {
      setPageState(replies["pageState"]);
    } else {
      setGotLastReply();
    }
    return this.getRemarksFromIds(
      new List<String>.from(
        replies["replies"] ?? [],
      ),
    );
  }

  Future<List<RemarkModel>> getUserOwnedRemarks(
    int length,
    Function setPageState,
    Function setGotLastReply, {
    String pageState = "",
    String username = "",
  }) async {
    Uri remarksUri;
    Map<String, String> qp;

    if (pageState.isNotEmpty) {
      if (username.isNotEmpty) {
        qp = {
          "length": length.toString(),
          "username": username,
          "pageState": pageState,
        };
      } else {
        qp = {
          "length": length.toString(),
          "pageState": pageState,
        };
      }
    } else {
      if (username.isNotEmpty) {
        qp = {
          "length": length.toString(),
          "username": username,
        };
      } else {
        qp = {
          "length": length.toString(),
        };
      }
    }
    remarksUri = ApiLoc.getUri(segments: ["remark", "user"], qp: qp);

    var response = await http.get(
      remarksUri,
      headers: {
        "session-token": await Utilities.getSessionToken(),
      },
    );

    Map<String, dynamic> remarks = jsonDecode(response.body);
    if (remarks["pageState"] != null) {
      setPageState(remarks["pageState"]);
    } else {
      setGotLastReply();
    }
    return this.getRemarksFromIds(new List<String>.from(remarks["remarks"]));
  }

  Future<RemarkModel> createRemarkRecord(RemarkModel model) async {
    try {
      var response = await http.post(
        ApiLoc.getUri(segments: ["remark"]),
        body: model.toJson(),
        headers: {
          "session-token": await Utilities.getSessionToken(),
          "content-type": "application/json",
        },
      );
      var responseParsed = RemarkModel.fromJson(response.body);
      return responseParsed;
    } catch (e) {
      print(e);
      return RemarkModel();
    }
  }

  Future<bool> uploadFile(UploadFile? file, String remarkId) async {
    if (file == null) {
      return false;
    }

    var uri = ApiLoc.getUri(
      segments: ["remark", "file"],
      qp: {"remarkId": remarkId},
    );
    var request = new http.MultipartRequest(
      "POST",
      uri,
    );

    request.headers.addAll({
      "session-token": await Utilities.getSessionToken(),
    });

    var fileRequest = new http.MultipartFile.fromBytes(
      "image",
      file.bytes!,
      filename: file.name,
    );

    request.files.add(fileRequest);
    var response = await request.send();

    if (response.statusCode != 200) {
      return false;
    } else {
      return true;
    }
  }

  Future<List<RemarkModel>> getRemarksFromIds(List<String> remarkIds) async {
    List<Future<Response>> requests = [];
    for (var remarkId in remarkIds) {
      var uri = ApiLoc.getUri(
        segments: ["remark"],
        qp: {
          "remarkId": remarkId,
        },
      );
      requests.add(http.get(uri));
    }

    var responses = await Future.wait(requests);

    List<RemarkModel> remarks = [];
    for (var response in responses) {
      remarks.add(RemarkModel.fromJson(response.body));
    }

    return remarks;
  }

  Future<RemarkModel> getSingleRemark(String remarkId) async {
    var url = ApiLoc.getUri(segments: ["remark"], qp: {"remarkId": remarkId});
    var response = await http.get(url);
    var decodedResponse = RemarkModel.fromJson(response.body);
    return decodedResponse;
  }

  Future<bool> deleteRemark(String remarkId) async {
    Uri uri = ApiLoc.getUri(
      segments: ["remark"],
      qp: {"remarkId": remarkId},
    );
    try {
      var response = await http.delete(
        uri,
        headers: {
          "session-token": await Utilities.getSessionToken(),
        },
      );
      if (response.statusCode != 200) {
        return false;
      } else {
        return true;
      }
    } catch (e) {
      return false;
    }
  }
}

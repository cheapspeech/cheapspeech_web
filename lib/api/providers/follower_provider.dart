import 'dart:convert';

import 'package:cheapspeech_web/api/models/follower_model.dart';
import 'package:cheapspeech_web/api/shared/api_locations.dart';
import 'package:cheapspeech_web/api/shared/utilities.dart';
import 'package:http/http.dart' as http;

class FollowerProvider {
  Future<bool> followUser(String username) async {
    var url = ApiLoc.getUri(
      segments: ["follow", "follow"],
    );

    var response = await http.post(
      url,
      body: jsonEncode({"username": username}),
      headers: {
        "session-token": await Utilities.getSessionToken(),
        "content-type": "application/json",
      },
    );
    return response.statusCode == 200;
  }

  Future<bool> unfollowUser(String username) async {
    var url = ApiLoc.getUri(
      segments: ["follow", "unfollow"],
    );

    var response = await http.post(
      url,
      body: jsonEncode({"username": username}),
      headers: {
        "session-token": await Utilities.getSessionToken(),
        "content-type": "application/json",
      },
    );
    return response.statusCode == 200;
  }

  Future<FollowerModel> getFollowerCount(String username) async {
    var url = ApiLoc.getUri(
      segments: ["follow", "count"],
      qp: {"username": username},
    );
    var response = await http.get(
      url,
      headers: {
        "session-token": await Utilities.getSessionToken(),
      },
    );
    var decodedResponse = FollowerModel.fromJson(response.body);
    return decodedResponse;
  }

  Future<FollowerModel> getFollowedUsers() async {
    var url = ApiLoc.getUri(segments: ["follow", "following"]);
    var response = await http.get(
      url,
      headers: {
        "session-token": await Utilities.getSessionToken(),
      },
    );
    var decodedResponse = FollowerModel.fromJson(response.body);
    return decodedResponse;
  }

  Future<FollowerModel> getIsFollowingUser(String username) async {
    var url = ApiLoc.getUri(
      segments: ["follow", "isFollowing"],
      qp: {
        "username": username,
      },
    );
    var response = await http.get(
      url,
      headers: {
        "session-token": await Utilities.getSessionToken(),
      },
    );
    var decodedResponse = FollowerModel.fromJson(response.body);
    return decodedResponse;
  }
}

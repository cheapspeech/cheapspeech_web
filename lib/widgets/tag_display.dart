import 'package:flutter/material.dart';

typedef TagDisplayCallback = void Function(String);
typedef OnTapListener = void Function(String);

class TagDisplay extends StatelessWidget {
  final List<String> tags;
  final bool allowTagRemoval;
  final TagDisplayCallback? removeTag;
  final OnTapListener? onTagTap;
  final bool displayHashTags;

  TagDisplay({
    required this.tags,
    required this.allowTagRemoval,
    this.removeTag,
    this.onTagTap,
    this.displayHashTags = true,
  });

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: this.makeTagCards(),
    );
  }

  List<Widget> makeTagCards() {
    List<Widget> widgets = [];
    this.tags.forEach((element) {
      var textToDisplay = this.displayHashTags ? "#" + element : element;
      List<Widget> cardChildren = [
        Text(
          textToDisplay,
          style: TextStyle(
            fontSize: 16,
          ),
        )
      ];
      if (this.allowTagRemoval) {
        cardChildren.add(this.makeTagRemovalButton(element));
      }
      if (this.onTagTap == null) {
        widgets.add(this.makeCard(cardChildren));
      } else {
        widgets.add(this.makeTappableCard(cardChildren, element));
      }
    });
    return widgets;
  }

  Widget makeTagRemovalButton(String element) {
    return SizedBox(
      height: 30,
      width: 30,
      child: FittedBox(
        child: TextButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              Colors.red,
            ),
            shape: MaterialStateProperty.all(
              CircleBorder(),
            ),
          ),
          onPressed: () {
            this.removeTag?.call(element);
          },
          child: Center(
            child: Icon(
              Icons.remove,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Widget makeCard(List<Widget> children) {
    return Card(
      semanticContainer: true,
      child: Padding(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: children,
        ),
        padding: EdgeInsets.all(5),
      ),
    );
  }

  Widget makeTappableCard(List<Widget> children, String elem) {
    return InkWell(
      onTap: () => this.onTagTap!.call(elem),
      child: Card(
        semanticContainer: true,
        child: Padding(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: children,
          ),
          padding: EdgeInsets.all(5),
        ),
      ),
    );
  }
}

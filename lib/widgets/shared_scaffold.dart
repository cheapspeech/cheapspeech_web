import 'package:cheapspeech_web/global_state/global_controller.dart';
import 'package:cheapspeech_web/pages/login/login_model.dart';
import 'package:cheapspeech_web/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cheapspeech_web/widgets/dialogs.dart';

class SharedScaffoldFactory {
  static Scaffold get({
    required Widget body,
    required BuildContext context,
    bool showBackArrow = false,
  }) {
    return Scaffold(
      appBar: AppBar(
        title: InkWell(
          child: Text("Cheap Speech *Beta*"),
          onTap: () => Navigator.of(context)
              .pushNamedAndRemoveUntil(Routes.home, (route) => false),
        ),
        automaticallyImplyLeading: false,
        leading: SharedScaffoldFactory.makeLeading(context, showBackArrow),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
            tooltip: "Search",
          ),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              var loggedIn =
                  Provider.of<GlobalController>(context, listen: false)
                      .model
                      .isLoggedIn;
              if (!loggedIn) {
                Dialogs.basicDialog("Please login to create a post.", context);
              } else if (ModalRoute.of(context)?.settings.name ==
                  Routes.create) {
                Navigator.of(context).popAndPushNamed(Routes.create);
              } else {
                Navigator.of(context).pushNamed(Routes.create);
              }
            },
            tooltip: "Create",
          ),
          Builder(
            builder: (context) {
              return IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  Scaffold.of(context).openEndDrawer();
                },
                tooltip: "Menu",
              );
            },
          ),
        ],
      ),
      body: body,
      endDrawer: Consumer<GlobalController>(
        builder: (context, controller, child) {
          return SharedScaffoldFactory.makeDrawer(
            context,
            controller.model.isLoggedIn,
          );
        },
      ),
    );
  }

  static Drawer makeDrawer(BuildContext context, bool isUserLoggedIn) {
    List<ListTile> drawerIcons = [
      ListTile(
        title: Text("Home"),
        leading: Icon(Icons.home),
        onTap: () {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(Routes.home, (route) => false);
        },
      ),
    ];
    if (isUserLoggedIn) {
      drawerIcons.addAll([
        ListTile(
          title: Text("Profile"),
          leading: Icon(Icons.account_circle),
          onTap: () => SharedScaffoldFactory.goToProfilePage(context),
        ),
        ListTile(
          title: Text("Logout"),
          leading: Icon(Icons.logout),
          onTap: () {
            Provider.of<GlobalController>(context, listen: false).logout();
            Navigator.of(context).pop();
          },
        ),
      ]);
    } else {
      drawerIcons.addAll([
        ListTile(
          title: Text("Login"),
          leading: Icon(Icons.login),
          onTap: () => SharedScaffoldFactory.goToLoginPage(
              LoginPageTasks.login, context),
        ),
        ListTile(
          title: Text("Sign Up"),
          leading: Icon(Icons.app_registration),
          onTap: () => SharedScaffoldFactory.goToLoginPage(
              LoginPageTasks.signup, context),
        ),
      ]);
    }

    // Home icon should be at the beginning of the list no matter what.
    drawerIcons.add(
      ListTile(
        title: Text("About Us"),
        leading: Icon(Icons.emoji_people),
        onTap: () => Navigator.of(context).pushNamed(Routes.welcome),
      ),
    );

    return Drawer(
        child: ListView(
      children: drawerIcons,
    ));
  }

  static void goToLoginPage(LoginPageTasks task, BuildContext context) {
    var nav = Navigator.of(context);
    nav.pop();
    if (ModalRoute.of(context)?.settings.name == Routes.login) {
      nav.popAndPushNamed(Routes.login, arguments: task);
    } else {
      nav.pushNamed(Routes.login, arguments: task);
    }
  }

  static void goToProfilePage(BuildContext context) {
    var nav = Navigator.of(context);
    nav.pop();
    if (ModalRoute.of(context)?.settings.name == Routes.profile) {
      nav.popAndPushNamed(Routes.profile);
    } else {
      nav.pushNamed(Routes.profile);
    }
  }

  static Widget makeLeading(BuildContext context, bool showBackArrow) {
    if (showBackArrow) {
      return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context),
      );
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }
}

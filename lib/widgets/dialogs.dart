import 'package:flutter/material.dart';

class Dialogs {
  static void basicDialog(String message, BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Page Alert"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SelectableText(message),
            SizedBox(height: 10),
            Row(
              children: [
                TextButton(
                  child: Text("Dismiss"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  static Future<dynamic> optionDialog({
    required String title,
    required String message,
    required BuildContext context,
    required List<String> options,
  }) async {
    if (options.length > 3) {
      throw new Exception("Only three options are currently supported by ui.");
    }

    var buttons = options.map((e) {
      return TextButton(
        child: Text(e),
        onPressed: () {
          Navigator.of(context).pop(e);
        },
      );
    }).toList();

    var option = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(message),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: buttons,
            )
          ],
        ),
      ),
    );

    return option;
  }
}

import 'package:cheapspeech_web/api/models/remark_model.dart';
import 'package:cheapspeech_web/widgets/tag_display.dart';
import 'package:flutter/material.dart';

typedef RemarkModelCallback = Future<List<RemarkModel>> Function();

class RemarkList extends StatefulWidget {
  final Function(RemarkModel) remarkSelected;
  final RemarkModelCallback getRemarks;
  final bool shrinkWrap;
  final double maxTileWidth;
  final Widget? body;

  RemarkList({
    Key? key,
    required this.remarkSelected,
    required this.getRemarks,
    this.shrinkWrap = false,
    this.maxTileWidth = 800,
    this.body,
  }) : super();

  @override
  RemarkListState createState() {
    return RemarkListState();
  }
}

class RemarkListState extends State<RemarkList> {
  List<RemarkModel> fetchedRemarks = [];
  bool recievedLastRemark = false;

  @override
  Widget build(BuildContext ctx) {
    return createInfiniteScroll(ctx);
  }

  Widget createInfiniteScroll(BuildContext ctx) {
    // Include 1 extra space for the loading circle or two extra spaces
    // for the loading circle and the page body.
    int itemCountOffset = (this.widget.body != null) ? 2 : 1;
    return new Scrollbar(
      child: ListView.builder(
        itemBuilder: (ctx, index) {
          var trueIndex = this.widget.body == null ? index : index - 1;
          // Append body of page if it exists.
          if (index == 0 && this.widget.body != null) {
            return this.widget.body!;
          }
          // Add new tiles if fetched tiles have not been expended.
          else if (trueIndex < fetchedRemarks.length) {
            return this.createPostTile(fetchedRemarks[trueIndex], ctx);
          }
          // If last remark has been recieved
          else if (this.recievedLastRemark) {
            return Center(
              child: Padding(
                child: SelectableText(
                  "Congratulations, you reached the end of this thread!",
                ),
                padding: EdgeInsets.only(
                  top: 20.0,
                  bottom: 20.0,
                ),
              ),
            );
          }
          // Fetch new tiles
          else {
            this._fetch(index);
            return Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
        itemCount: fetchedRemarks.length + itemCountOffset,
      ),
    );
  }

  Widget createPostTile(RemarkModel item, BuildContext ctx) {
    var screenWidth = MediaQuery.of(ctx).size.width;
    var minimumTotalInsetSpace = 40;
    var availableInsetSpace = screenWidth - this.widget.maxTileWidth;
    var tileWidth = availableInsetSpace >= minimumTotalInsetSpace
        ? this.widget.maxTileWidth
        : screenWidth - minimumTotalInsetSpace;

    var tags = item.tags ?? [];
    var tagsToDisplay = tags.length >= 8 ? tags.sublist(0, 8) : tags;

    return Column(
      children: [
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MouseRegion(
              cursor: SystemMouseCursors.click,
              child: InkWell(
                onTap: () {
                  this.widget.remarkSelected(item);
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                    ),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  width: tileWidth,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Text(
                            item.title ?? "Title not found.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                      TagDisplay(
                        tags: tagsToDisplay,
                        allowTagRemoval: false,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  String trimText(String text) {
    var textLength = 200;
    if (text.length > textLength) {
      return text.substring(0, textLength) + "...";
    } else {
      return text;
    }
  }

  Future<void>? _fetch(int pageKey) async {
    var remarks = await this.widget.getRemarks();
    if (remarks.isEmpty) {
      this.setState(() {
        this.recievedLastRemark = true;
      });
    } else {
      this.setState(() {
        this.fetchedRemarks.addAll(remarks);
      });
    }
  }
}

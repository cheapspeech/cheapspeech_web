import 'package:cheapspeech_web/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'global_state/global_controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        var controller = new GlobalController();
        controller.initializeGlobalStateAsync();
        return controller;
      },
      child: MaterialApp(
        title: 'Cheap Speech',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        initialRoute: Routes.home,
        routes: Routes.routes,
      ),
    );
  }
}

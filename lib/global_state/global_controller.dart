import 'package:cheapspeech_web/api/providers/user_provider.dart';
import 'package:cheapspeech_web/api/shared/utilities.dart';
import 'package:cheapspeech_web/global_state/global_model.dart';
import 'package:flutter/widgets.dart';

class GlobalController with ChangeNotifier {
  GlobalModel model = new GlobalModel();

  Future<void> initializeGlobalStateAsync() async {
    if ((await Utilities.getSessionToken()).isNotEmpty) {
      this.model.isLoggedIn = true;
    }

    notifyListeners();
  }

  Future<bool> login(String username, String password) async {
    var apiProvider = new UserProvider();
    var successful = await apiProvider.login(username, password);
    this.model.isLoggedIn = successful;
    notifyListeners();
    return successful;
  }

  Future<void> logout() async {
    var apiProvider = new UserProvider();
    await apiProvider.logout();
    this.model.isLoggedIn = false;
    notifyListeners();
  }

  Future<bool> createAccount(String username, String password) async {
    var apiProvider = new UserProvider();
    var success = await apiProvider.createAccount(username, password);
    this.model.isLoggedIn = success;
    notifyListeners();
    return success;
  }
}
